
/* Configuration */

// ALL TIME VARIABLES SHOULD BE iN MILLISECONDS!
var seconds = 1000; // seconds to ms

var target_fpms = 25 * seconds; // The target frame rate of the result
var record_fpms = 10 * seconds; // phantomjs will not want to work very fast
var rate = target_fpms / record_fpms; // How much more slowly will animations need to be?

var fade_in_time = 2 * seconds * rate;
var persist_time = 60 * seconds * rate;
var fade_out_time = 5 * seconds * rate;

/* Step 0. Populate user mapping */

var users = {};

var Connect = new XMLHttpRequest();
Connect.open("GET", "users.xml", false);
Connect.setRequestHeader("Content-Type", "text/xml");
Connect.send(null);

$.each(Connect.responseXML.childNodes[0].children, function(index, obj) {
    users[obj.attributes.name.nodeValue] = {
        username: obj.attributes.username.nodeValue,
        name: obj.attributes.real_name.nodeValue,
    };
});

/* Step 1. Populate the messages from a big blue button xml file */

var messages = new Array(); // Each message itself, ordered by their starting time (evident in the xml)
var actions = new Array(); // Each fade in and out ordered by it's start time (two per message)

var Connect = new XMLHttpRequest();
Connect.open("GET", "slides_new.xml", false);
Connect.setRequestHeader("Content-Type", "text/xml");
Connect.send(null);

$.each(Connect.responseXML.childNodes[0].children, function(index, obj) {
    var _in = parseInt(obj.attributes.in.nodeValue) * seconds * rate;
    var name = obj.attributes.name.nodeValue;

    if(users[name] == undefined) {
        users[name] = {
            username: name,
            name: name,
        };
    }

    var current = {
        mid: "msg" + index.toString(),
        user: users[name],
        msg: obj.attributes.message.nodeValue,
        submsg: new Array(),
        // Absolute times of events
        fade_in: _in,
        fade_out: _in + persist_time,
    }
    current.object_id = '.' + current.mid;

    if(messages.length) {
        var previous = messages[messages.length - 1];
        // Detect if two messages next to each other are from the same author.
        //  AND the messages are close enough together to persist at the same time.
        if(previous.user.username == current.user.username && previous.fade_out > current.fade_in) {
            // Add a new line to the message after this delay. Time is RELATIVE
            previous.submsg.push({
                mid: current.mid,
                msg: current.msg,
                object_id: current.object_id,
                fade_in: current.fade_in,
            });
            // Extend lifetime of the bubble.
            previous.fade_out = current.fade_out;
            // Do not add a new messages element
            return;
        }
    }
    messages.push(current);
});

for(let i = 0; i < messages.length; i++) {
    var current = messages[i];
    actions.push({method: 'fade_in', time: current.fade_in, obj: current.object_id});
    actions.push({method: 'fade_out', time: current.fade_out, obj: current.object_id});
    for(let j = 0; j < current.submsg.length; j++) {
        var sub = current.submsg[j];
        actions.push({method: 'fade_in', time: sub.fade_in, obj: sub.object_id});
    }
}

function compare_time(a, b) {
  if (a.time > b.time) return 1;
  if (b.time > a.time) return -1;
  return 0;
}
actions.sort(compare_time);

/* Set session data for phantomjs to read */
document.session = {
    rate: rate,
    fps: record_fpms / seconds,
    length: messages.length,
    start: Date.now(), // Time current action started
    changed: Date.now(), // Last time the document changed
    offset: 0, // Offset of current action
    current: 0, // Now minus Start plus offset indicating likely frame
    complete: false,
    completed: actions[actions.length-1].time + (5 * seconds * rate),
};

function onRowAdded(index, obj, msgname) {
    next_active = index + 1;
    $('.chat-container').animate({
        scrollTop: $('.chat-container').prop('scrollHeight')
    });
};

$(document).ready(function() {
    $.each(messages, function(index, obj) {
        $(".chat-message-list").append("<li class='message-right " + obj.mid + "' hidden><div class='sp-" + obj.mid + "'></div><div class='messageinner-" + obj.mid + "'><span class='message-author'><span class='inner-author'><span class='author-avatar'><img src='avatars/"+obj.user.username+".png' onerror='this.onerror=null; this.src=\"avatars/default.png\"'/></span>" + obj.user.name + "</span></span><span class='message-text'>" + obj.msg + "</span></div></li>");
        for(let j = 0; j < obj.submsg.length; j++) {
            var sub = obj.submsg[j];
            $('.message-text', obj.object_id).append("<div class='sub-message " + sub.mid + "'>" + sub.msg + "</div>");
            $(sub.object_id).hide();
        }
    });

    var next_action = 0;
    setInterval(function() {
        if(next_action >= actions.length) {
            document.title = 'Finished';
            document.session.complete = true;
            return;
        }
        var last_change = (Date.now() - document.session.changed);
        if(last_change > 1000) {
            // skip to next action time
            document.session.offset = actions[next_action].time;
            document.session.start = Date.now();
            //console.log("Skipping forwards", document.session.offset);
        }
        // Set the current time clock, make sure it never slips backwards
        var new_time = (Date.now() - document.session.start) + document.session.offset;
        if(new_time > document.session.current) { document.session.current = new_time; }
        //document.title = document.session.current / seconds / rate;
        // Should we execute the next action, either because the existing animation has pushed into it
        // or because nothing has changed for a while and we have advanced.
        if(actions[next_action].time <= document.session.current) {
            var action = actions[next_action];
            //console.log("Next action!", action);
            var current_time = Date.now();
            if(action.method == 'fade_in') {
                $(action.obj).fadeIn(fade_in_time);
                current_time = new Date(current_time + fade_in_time + 100);
            } else if(action.method == 'fade_out') {
                $(action.obj).fadeOut(fade_out_time);
                current_time = new Date(current_time + fade_out_time + 100);
            }
            document.session.changed = current_time;
            next_action += 1;
        }
    }, 10);
});
