
# Turn Chat into Video

When recording from things like Big Blue button, you will get a presentation which is not uploadable to other sites such as youtube.

To get around the limitations of the chat portion of the information, this project renders the chat as a constant stream of animated bubbles aligned to the right of the screen.

These frames can then be overlayed on top of your video in the configuration desired.

# Instructions

Firstly download the slides_new.xml file from the presentation (in future we may automate this).

Install phantomjs for your computer

Modify the css if needed to get the look you want. test in Firefox or Chrome by opening the sr.html file.

Run `phantomjs psr.js` to generate a folder called `frames` with all the frames needed.

Use the `fill_in_frames.py` script to populate a full set of frames if your editor needs each frame index to exist.

# Notes

You can customise the avatars by placing them in the avatars directory using the same name as they used in the chat.

The default frame-rate is 2.5fps, in your video editor specify 25 frames per ten seconds. You can adjust the number of frames you need in the javascript file `sr.js`.

Do not convert to an mpeg or other video format as this will destroy the transparency.

