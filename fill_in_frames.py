#!/usr/bin/env python3
#
# Loops through all the frames and copies to fill in the missing holes.
#

import re
import os
import sys

def generate_path_def(filename):
    """Generate a regex which tightens the matching on a frame path"""
    # Step 1. Find all numbers in the filename
    spans = [match.span() for match in re.finditer('[0-9]+', filename)]
    # Step 2. Sort the numbers by size, pick the largest one
    span = sorted(spans, key=lambda x: x[1] - x[0])[-1]
    # Step 3. Return the two fragments and size of the number
    return filename[:span[0]], span, filename[span[1]:]


def main(path):
    if not os.path.isdir(path):
        sys.stderr.write(f"Can not find path: {path}\n")
        return -2

    # Step 1. Generate a list of frames
    frames = []
    (start, span, end) = None, None, None
    for item in os.listdir(path):
        if start is None:
            (start, span, end) = generate_path_def(item)
        if item.startswith(start) and item.endswith(end):
            frames.append(int(item[span[0]:span[1]]))
            num = int(item[span[0]:span[1]])
        else:
            sys.stderr.write(f" ! Ignoring {item}")

    # Step 2. Sort the list of frames
    frames.sort()
    length = span[1] - span[0]
    template = f'{{:0{length}}}'
    def gen_filepath(num):
        """Generate a filepath from the frame number"""
        filename = start + template.format(num) + end
        return os.path.join(path, filename)

    # Step 3. Loop through all the frames two at a time and fill in the holes
    last_frame = None
    for frame_num in range(1, frames[-1] + 1):
        filepath = gen_filepath(frame_num)
        if os.path.isfile(filepath):
            if last_frame is None and frame_num != 1:
                # Special case, back fill frames until frame 1 is reached
                for back_frame_num in range(frame_num - 1, 0, -1):
                    sys.stderr.write(f" < Backfilling frame {back_frame_num} with {filepath}\n")
                    os.link(filepath, gen_filepath(back_frame_num))
            last_frame = filepath
        elif last_frame is not None:
            os.link(last_frame, filepath)
            sys.stderr.write(f" > Filled frame {frame_num} with {last_frame}\n")
    return 0

if __name__ == '__main__':
    if len(sys.argv) > 1:
        sys.exit(main(sys.argv[-1]))
    sys.exit(main('frames'))

