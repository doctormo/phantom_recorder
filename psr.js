var sys = require('system');
var fs = require('fs');

var page = require('webpage').create();
var loadInProgress = true;

page.onLoadStarted = function() { loadInProgress = true };
page.onLoadFinished = function() { loadInProgress = false };
page.onConsoleMessage = function(msg, lineNum, sourceId) {
  console.log('CONSOLE: ' + msg + ' (from line #' + lineNum + ' in "' + sourceId + '")');
};
page.onResourceReceived = function(res) {
  if (res.stage === 'end' && res.status != 2001 && res.status != 3021) {
    console.log("LOAD:" + res.url + ', Status code: ' + res.status);
  }
};

var current_frame = 0;
var final_frame = 0;
var recorded = 0;

String.prototype.padStart = function(padString, length) {
    var str = this;
    while (str.length < length)
        str = padString + str;
    return str;
}

page.viewportSize = { width: 300, height: 900 };

page.open('file://' + fs.absolute('sr.html'), function(status) {
    if (status != 'success') {
        console.error("FAILED!");
        phantom.exit();
    }
    setInterval(function() {
        if(loadInProgress) { return; }
        var session = page.evaluate(function() { return document.session; });
        if(!session) { return; }

        // Calculate how much time has passed
        var time = session.current / session.rate;
        var timeString = Math.floor(time / 1000);

        // Work out what frame we would be by the time passed
        var frame_time = Math.floor(time / (1000 / session.fps));
        var frame_num = frame_time.toString().padStart("0", 6);

        if(final_frame == 0) {
            final_frame = (session.completed / session.rate) / (1000 / session.fps);
            console.log("Final frame: " + final_frame);
        }

        if(frame_time > current_frame) {
            current_frame = frame_time;

            // And what percentage of the total number of frames is that?
            var pc = parseFloat((frame_time / final_frame) * 100).toFixed(2) + "%";

            // Work out the effective frames per second
            var calc_fps = parseFloat(current_frame / Math.floor(frame_time / session.fps)).toFixed(2) + "fps";

            recorded++;
            sys.stdout.write(
                "[" + timeString + "] " + pc + " (frame:" + current_frame + "/" + recorded + ") " + calc_fps + "\r"
            );
            page.render("frames/frame_" + frame_num + ".png", { format: "png" });
            if (frame_time > final_frame) {
                phantom.exit();
            }
        }
    }, 10);
});
